<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header>
    <div class="container position-relative">
		<div class="drop-menu">
			<nav id="MainNav" class="h-100">
				<?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
			</nav>
			<div class="header-socials">
				<?php if ($facebook = opt('facebook')) : ?>
				<a href="<?= $facebook; ?>" class="trigger-item trigger-item-fb">
					<i class="fab fa-facebook-f"></i>
				</a>
				<?php endif; ?>
				<?php if ($whatsapp = opt('whatsapp')) : ?>
					<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" class="trigger-item trigger-item-wa">
						<i class="fab fa-whatsapp"></i>
					</a>
				<?php endif;
				if ($instagram = opt('instagram')) : ?>
				<a href="<?= $instagram; ?>" class="trigger-item trigger-item-inst">
					<i class="fab fa-instagram"></i>
				</a>
				<?php endif; ?>
				<div class="trigger-item pop-trigger">
					<img src="<?= ICONS ?>pop.png" alt="open-popup">
				</div>
				<div class="search-header">
					<?php get_search_form(); ?>
				</div>
			</div>
		</div>
        <div class="row">
			<div class="col d-flex justify-content-start align-items-stretch">
				<div class="square-wrap menu-trigger">
					<button class="hamburger hamburger--spin" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<a class="add-property-btn" href="<?= opt('add_prop_link'); ?>">
					<span class="plus-icon">+</span>
					<span class="add-prop-text">פרסום נכס </span>
				</a>
				<div class="square-wrap wishlist-trigger">
					<img src="<?= ICONS ?>like-white.png" alt="wishlist">
				</div>
				<a class="square-wrap reg-wrap" href="<?php wp_registration_url(); ?>">
					<img src="<?= ICONS ?>user.png" alt="login">
					<span class="reg-title">כניסה</span>
				</a>
			</div>
			<?php if ($logo = opt('logo')) : ?>
				<div class="col-auto">
					<a href="/" class="logo">
						<img src="<?= $logo['url'] ?>" alt="logo">
					</a>
				</div>
			<?php endif; ?>
        </div>
    </div>
</header>


<section class="pop-form">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xxl-6 col-xl-6 col-lg-8 col-md-9 col-sm-10 col-12 d-flex justify-content-center">
				<div class="float-form d-flex flex-column align-items-center">
					<div class="form-wrapper-pop">
						<span class="close-form">
							<img src="<?= ICONS ?>close.png" alt="close">
						</span>
						<?php if ($f_title = opt('pop_form_title')) : ?>
							<h2 class="form-title"><?= $f_title; ?></h2>
						<?php endif;
						if ($f_subtitle = opt('pop_form_subtitle')) : ?>
							<h3 class="form-subtitle"><?= $f_subtitle; ?></h3>
						<?php endif;
						getForm('84'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
