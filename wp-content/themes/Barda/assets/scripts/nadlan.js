(function($) {
	$( document ).ready(function() {
		console.log('aaa');

		$('#inputAddress').select2({
			dir: 'rtl',
			language: 'he',
			ajax: {
				delay: 100,
				url: JSObject.wp_ajax,
				dataType: 'json',
				data: function (params) {
					var query = {
						action: 'location_search',
						q: params.term
					};
					return query;
				},
				processResults: function( response ) {
					return {
						results: response.data
					};
				},
				cache: true
			},
			minimumInputLength: 3
		});

	});
})( jQuery );
