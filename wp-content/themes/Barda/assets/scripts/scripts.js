(function($) {
	$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
	$( document ).ready(function() {
		var menu = $( '.drop-menu' );
		$('.hamburger').click(function () {
			menu.slideFadeToggle();
			$(this).toggleClass('is-active');
		});
		$('.menu-item-has-children').click(function () {
			$(this).children('.sub-menu').slideFadeToggle();
		});
		$('.btn-advanced').click(function(){
			$(this).parents().find('#advanced-form').slideFadeToggle();
			$(this).parents().find('.box-wrapper').toggleClass('no-box', '');
		});
		// if ($('.radio-check').is(':checked')) {
		// 	$(this).parent('.choose-option').addClass('checked-option');
		// }
		$('.radio-check').change(
			function(){
				if ($(this).is(':checked')) {
					$(this).parent('.choose-option').addClass('checked-option');
				} else {
					$(this).parent('.choose-option').removeClass('checked-option');
				}
			});
		// $('.social-copy').click(function() {
		// 	var $temp = $("<input>");
		// 	$("body").append($temp);
		// 	$temp.val($(this).text()).select();
		// 	document.execCommand("copy");
		// 	$temp.remove();
		// });
		$('.cat-item-img').hover(function (){
			$(this).children('.cat-item-info').children('.cat-item-info-part').slideFadeToggle();
		});
		$('.search-trigger').click(function () {
			$('.pop-search').addClass('show-search');
			$('.float-search').addClass('show-float-search');
		});
		$('.close-search').click(function () {
			$('.pop-search').removeClass('show-search');
			$('.float-search').removeClass('show-float-search');
		});
		$('.pop-trigger').click(function () {
			$('.pop-form').addClass('show-popup');
			$('.float-form').addClass('show-float-form');
			$('.pop-body').addClass('curr-body-hidden');
		});
		$('.close-form').click(function () {
			$('.pop-form').removeClass('show-popup');
			$('.float-form').removeClass('show-float-form');
			$('.pop-body').removeClass('curr-body-hidden');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$('.main-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: false,
			dots: true,
		});
		$('.gallery-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			dots: false,
			fade: true,
			rtl: true,
		});
		$('.prop-slider').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			rtl: true,
			dots: false,
			arrows: true,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-button').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		$('.play-button-post').click(function() {
			var id = $(this).data('id');
			var iFrame = '<iframe src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('.put-video-here').addClass('show').html(iFrame);
		});
		$('.accordion-faq').each(function(i, obj) {
			$(obj).on('shown.bs.collapse', function () {
				var show = $( '.show' );
				show.parent().children('.question-title').children('.faq-arrow').addClass('arrow-bottom');
				show.parent().children('.question-title').addClass('active-faq');
			});
			$(obj).on('hidden.bs.collapse', function () {
				var collapsed = $( '.collapse' );
				collapsed.parent().children('.question-title').children('.faq-arrow').removeClass('arrow-bottom');
				collapsed.parent().children('.question-title').removeClass('active-faq');
			});
		});

		//Socials
		$('.trigger-wrap').hover(function(){
			$(this).children('.all-socials').addClass('show-share');
		}, function(){
			$(this).children('.all-socials').removeClass('show-share');
		});
	});

	$('.load-more-posts').click(function() {
		var termID = $(this).data('term');
		// var params = $('.take-json').html();
		var ids = '';
		var video = $(this).data('content');
		var page = $(this).data('page');

		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				// postType: postType,
				termID: termID,
				ids: ids,
				page: page,
				video: video,
				// taxType: taxType,
				action: 'get_more_function',
			},
			success: function (data) {
				if (!data.html) {
					$('.load-more-posts').addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});

	$('.add-to-wishlist').click(function() {
		var wishID = $(this).data('id');
		// var params = $('.take-json').html();
		var ids = '';

		$('.wishlist-page .property-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				wishID: wishID,
				ids: ids,
				// page: page,
				action: 'add_to_wish',
			},
			success: function (data) {
				if (!data.html) {
					$(this).addClass('in-wishlist');
				}
				$('.put-here-wishlist').append(data.html);
			}
		});
	});

})( jQuery );
