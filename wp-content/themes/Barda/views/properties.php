<?php
/*
Template Name: נכסים
*/

get_header();
$fields = get_fields();
$_kibbutz = (isset($_GET['kibbutz_only'])) ? 1 : null;


$_location = (isset($_GET['location']) && !$_kibbutz) ? intval($_GET['location']) : null;
$_type = (isset($_GET['property-type'])) ? intval($_GET['property-type']) : null;
$_params = (isset($_GET['params'])) ? $_GET['params'] : null;



$_room_from = (isset($_GET['rooms-from'])) ? intval($_GET['rooms-from']) : 0;
$_room_to = (isset($_GET['rooms-to']) && !empty($_GET['rooms-to'])) ? intval($_GET['rooms-to']) : PHP_INT_MAX;

$_price_from = (isset($_GET['price-from'])) ? intval($_GET['price-from']) : 0;
$_price_to = (isset($_GET['price-to']) && !empty($_GET['price-to'])) ? intval($_GET['price-to']) : PHP_INT_MAX;


$_floor_from = (isset($_GET['floor-from'])) ? intval($_GET['floor-from']) : 0;
$_floor_to = (isset($_GET['floor-to']) && !empty($_GET['floor-to'])) ? intval($_GET['floor-to']) : PHP_INT_MAX;

$_size_from = (isset($_GET['size-from'])) ? intval($_GET['size-from']) : 0;
$_size_to = (isset($_GET['size-to']) && !empty($_GET['size-to'])) ? intval($_GET['size-to']) : PHP_INT_MAX;

$_date = (isset($_GET['property-date'])) ? $_GET['property-date'] : null;

$_immediately = (isset($_GET['property-date-immediately'])) ? 1 : null;

$_query = (isset($_GET['search-query'])) ? sanitize_text_field($_GET['search-query']) : null;


$query_args = [
	'post_type' => 'property',
	'posts_per_page' => -1,
	'meta_query' => [
		[
			'key' => 'rooms',
			'value'   => [$_room_from, $_room_to],
			'type'    => 'numeric',
			'compare' => 'BETWEEN',
		],
		[
			'key' => 'price',
			'value'   => [$_price_from, $_price_to],
			'type'    => 'numeric',
			'compare' => 'BETWEEN',
		],
		[
			'key' => 'floor',
			'value'   => [$_floor_from, $_floor_to],
			'type'    => 'numeric',
			'compare' => 'BETWEEN',
		],
		[
			'key' => 'square',
			'value'   => [$_size_from, $_size_to],
			'type'    => 'numeric',
			'compare' => 'BETWEEN',
		],
	]
];

if($_immediately){
	array_push($query_args['meta_query'], [
		'key' => 'free_date_bool',
		'value' => '1',
		'compare' => '=='
	]);
}elseif($_date){
	array_push($query_args['meta_query'], [
		'key' => 'free_date',
		'value' => $_date,
		'compare' => '>=',
		'type' => 'DATE'
	]);
}
if($_location || $_type || $_params){
	$query_args['tax_query'] = [
		'relation' => 'AND',
		$_location ? [
			'taxonomy' => 'location',
			'field'    => 'term_id',
			'terms'    => [$_location],
		] : null,
		$_type ? [
			'taxonomy' => 'property_type',
			'field'    => 'term_id',
			'terms'    => [$_type],
		] : null,
		$_params ? [
			'taxonomy' => 'param',
			'field'    => 'term_id',
			'terms'    => $_params,
		] : null,
	];
}else{
	$query_args['tax_query'] = null;
}


if($_kibbutz){
	$locations = get_terms([
		'taxonomy'      => 'location',
		'hide_empty'    => false,
		'parent'        => 0
	]);

	$_kibitz_terms = [];
	if($locations){
		foreach($locations as $loc){
			if(get_field('location_type', $loc) === 'kibbutz'){
				$_kibitz_terms[] = $loc->term_id;
			}
		}
	}

	if($_location) {

	}else{
		$query_args['tax_query'][] = [
			'taxonomy' => 'location',
			'field'    => 'term_id',
			'terms'    => $_kibitz_terms,
		];
	}
}

if($_query){
	$query_args['s'] = $_query;
}

$properties = new WP_Query($query_args);
get_template_part('views/partials/content', 'top', [
		'img' => (isset($fields['top_img']) && $fields['top_img']) ? $fields['top_img']['url'] : (has_post_thumbnail() ? postThumb() : ''),
]); ?>
<article class="properties-search-body mt-4">
	<?php if ($properties->have_posts()) : the_post(); $counter = 1; ?>
		<div class="post-list">
			<div class="container">
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($properties->posts as $x => $prop) : ?>
						<div class="col-12 property-col">
							<?php get_template_part('views/partials/card', 'property_inline', [
									'property' => $prop,
							]); ?>
						</div>
					<?php if(($counter % 7 === 0) || ($counter === count($properties->posts) && $counter <= 7)) : ?>
				</div>
			</div>
		</div>
				<!--Form-->
				<?php get_template_part('views/partials/repeat', 'property'); ?>
				<div class="post-list">
				<div class="container">
				<div class="row justify-content-center align-items-stretch">
			<?php endif; ?>
			<?php $counter++; ?>
		<?php endforeach; ?>
		</div>
		</div>
		</div>
	<?php endif; ?>
</article>

<?php get_footer(); ?>
