<?php if (isset($args['link']) && $args['link']) :
	$title = isset($args['title']) && $args['title'] ? $args['title'] : '';
	$type = isset($args['type']) && $args['type'] ? $args['type'] : 'post'; ?>
	<div class="share-block">
		<h3 class="share-text">
			<?= ($type == 'prop') ? 'שתף את המודעה' : 'שתפו את המאמר'; ?>
		</h3>
		<div class="socials-share">
			<!--	MAIL-->
			<a href="mailto:?subject=&body=<?= $args['link']; ?>" target="_blank" class="social-share-link">
				<img src="<?= ICONS ?>share-mail.png" alt="email">
			</a>
			<!--	COPY LINK-->
			<a href="<?= $args['link']; ?>" class="social-share-link social-copy">
				<img src="<?= ICONS ?>share-link.png" alt="copy-link">
			</a>
			<!--	WHATSAPP-->
			<a href="https://api.whatsapp.com/send?text=<?= $title.' '.$args['link']; ?>"
			   class="social-share-link wow fadeInUp" data-wow-delay="0.6s">
				<img src="<?= ICONS ?>share-whatsapp.png" alt="whatsapp">
			</a>
			<!--	PRINT-->
			<a href="<?= $args['link']; ?>" onclick="window.print();return false;" class="social-share-link">
				<img src="<?= ICONS ?>share-fax.png" alt="print-document">
			</a>
		</div>
		<div class="report-block">
			<?php if ($rep_link = opt('report_link')) : ?>
				<a href="<?= $rep_link; ?>" target="_blank" class="base-link">
					מצאתי טעות במודעה
				</a>
			<?php elseif ($mail = opt('mail')) : ?>
				<a href="mailto:<?= $mail; ?>" target="_blank" class="base-link">
					מצאתי טעות במודעה
				</a>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>
