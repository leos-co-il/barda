<?php if (isset($args['category']) && $args['category']) : $link = get_category_link($args['category']); ?>
	<div class="col-lg-4 col-md-6 col-sm-10 col-12 col-post">
		<div class="post-card more-card box-wrapper category-card">
			<a class="post-img" <?php if ($img = get_field('cat_img', $args['category']->term_id)) : ?>
				style="background-image: url('<?= $img['url']; ?>')"
			<?php endif; ?> href="<?= $link; ?>">
			</a>
			<div class="post-card-content">
				<div class="d-flex flex-column justify-content-start align-items-start">
					<a class="post-card-title" href="<?= $link; ?>"><?= $args['category']->name; ?></a>
					<p class="card-text">
						<?= text_preview(category_description($args['category']), 10); ?>
					</p>
				</div>
				<a class="base-link category-card-link" href="<?= $link; ?>">
					לכל הנכסים
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
