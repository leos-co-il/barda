<?php if (isset($args['faq']) && $args['faq']) : ?>
	<div class="faq">
		<div class="container">
			<div class="row justify-content-start">
				<div class="col-auto">
					<h2 class="block-title">
						<?= (isset($args['title']) && $args['title']) ? $args['title'] : 'שאלתם? ענינו!'; ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-12">
					<?php foreach ($args['faq'] as $num_acc => $acc_faq) : if (isset($acc_faq['faq_item']) && $acc_faq['faq_item']) : ?>
						<div class="box-wrapper faq-box-wrapper">
							<h3 class="acc-block-title mb-3"><?= isset($acc_faq['faq_topic']) ? $acc_faq['faq_topic'] : ''; ?></h3>
							<div id="accordion-<?= $num_acc; ?>" class="accordion-faq">
								<?php foreach ($acc_faq['faq_item'] as $num => $item) : ?>
									<div class="card question-card">
										<div class="question-header" id="heading_<?= $num_acc.$num; ?>">
											<button class="question-title" data-toggle="collapse"
													data-target="#faqChild<?= $num_acc.$num; ?>"
													aria-expanded="false" aria-controls="collapseOne">
												<span class="faq-body-title"><?= $item['faq_question']; ?></span>
												<img src="<?= ICONS ?>faq.png" class="faq-arrow" alt="arrow-bottom">
											</button>
											<div id="faqChild<?= $num_acc.$num; ?>" class="collapse faq-item answer-body"
												 aria-labelledby="heading_<?= $num_acc.$num; ?>" data-parent="#accordion-<?= $num_acc; ?>">
												<div class="base-output">
													<?= $item['faq_answer']; ?>
												</div>
											</div>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					<?php endif; endforeach; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
