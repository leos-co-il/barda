<?php $props = (isset($args['items']) && $args['items']) ? $args['items'] : opt('pop_props');
$props_title = (isset($args['title']) && $args['title']) ? $args['title'] : opt('pop_props_title');
$link = (isset($args['link']) && $args['link']) ? $args['link'] : opt('search_page');
if ($props) : ?>
	<div class="slider-of-properties prop-slider-back">
		<div class="container">
			<div class="row justify-content-start">
				<div class="col-auto">
					<h2 class="block-title">
						<?= (isset($args['title']) && $args['title']) ? $args['title'] : ''; ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center arrows-slider arrows-slider-sides">
				<div class="col-12">
					<div class="prop-slider" dir="rtl">
						<?php foreach ($props as $prop): ?>
							<div class="p-1">
								<?php get_template_part('views/partials/card', 'property',
										[
												'property' => $prop,
									]);
								?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
			<?php if ($link) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<a href="<?= $link['url'];?>" class="base-link">
							<?= (isset($link['title']) && $link['title'])
									? $link['title'] : 'לכל הנכסים';
							?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>
