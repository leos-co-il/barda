<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-md-4 col-sm-10 col-12 col-post">
		<div class="post-card more-card box-wrapper category-card" data-id="<?= $args['post']->ID; ?>">
			<a class="post-img" <?php if (has_post_thumbnail($args['post'])) : ?>
				style="background-image: url('<?= postThumb($args['post']); ?>')"
			<?php endif; ?> href="<?= $link; ?>">
			</a>
			<div class="post-card-content">
				<div class="d-flex flex-column justify-content-start align-items-start">
					<a class="post-card-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
					<p class="card-text">
						<?= text_preview($args['post']->post_content, 10); ?>
					</p>
				</div>
				<a class="base-link category-card-link" href="<?= $link; ?>">
					המשך קריאה
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
