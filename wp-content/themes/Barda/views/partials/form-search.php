<?php

$params = get_terms([
	'taxonomy' => 'param',
	'hide_empty' => false,
]);
$cats = get_terms([
	'taxonomy' => 'property_type',
	'hide_empty' => false,
]);
$cats_main = get_terms([
		'taxonomy' => 'property_cat',
		'hide_empty' => false,
		'parent' => 0
]);

?>

<div class="property-search-section">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="property-search box-wrapper">
					<div class="cats-top-over">
						<?php if ($cats_main) : foreach($cats_main as $cat_link) : ?>
							<a href="<?= get_term_link($cat_link); ?>" class="cat-top-link">
								<?= $cat_link->name; ?>
							</a>
						<?php endforeach; endif; ?>
						<a class="add-property-btn cat-top-link" href="<?= opt('add_prop_link'); ?>">
							<span class="plus-icon">+</span>
							<span class="add-prop-text">פרסום נכס </span>
						</a>
					</div>
					<form method="get" action="<?= opt('search_page') ?>">
						<div class="row align-items-end">
							<div class="form-group col-md-6 col-xl-3 col-xxl-4">
								<label for="inputAddress"><?= esc_html__('חפשו אזור, עיר, שכונה או רחוב','leos') ?></label>
								<div class="input-group form-control p-0">
									<select  class="form-control" name="location" id="inputAddress">
									</select>
								</div>
							</div>
							<?php if($cats): ?>
								<div class="form-group col-md-6 col-xl">
									<label for="inputType"><?= esc_html__('סוג הנכס','leos') ?></label>
									<select id="inputType" name="property-type" class="form-control">
										<option selected><?= esc_html__('בחרו סוגי נכסים','leos')  ?></option>
										<?php foreach($cats as $c): ?>
											<option value="<?= $c->term_id ?>"><?= $c->name ?></option>
										<?php endforeach ?>
									</select>
								</div>
							<?php endif; ?>
							<div class="form-group col-md-3 col-xl col-xxl-1">
								<label for="inputRooms"><?= esc_html__('חדרים','leos') ?></label>
								<div class="input-group">
									<input type="text" class="form-control"  name="rooms-from" id="inputRooms" placeholder="מ-">
									<input type="text" class="form-control"  name="rooms-to" id="inputRooms2"  placeholder="עד-">
								</div>
							</div>
							<div class="form-group col-md-3 col-xl price-group">
								<label for="inputPrice"><?= esc_html__('טווח מחירים בש”ח','leos') ?></label>
								<div class="input-group">
									<input type="text" class="form-control" id="inputPrice" name="price-from" placeholder="ממחיר">
									<input type="text" class="form-control" id="inputPrice2" name="price-to" placeholder="עד מחיר">
								</div>
							</div>
							<div class="form-group col-md-6 col-xl-auto d-flex align-items-end">
								<div class="input-group">
									<button type="submit" class="btn btn-primary btn-search"><?= esc_html__('חיפוש','leos') ?></button>
									<button type="button" class="btn btn-primary btn-advanced"><?= esc_html__('חיפוש מתקדם','leos') ?></button>
								</div>
							</div>
						</div>

						<div id="advanced-form">
							<h5 class="search-title-group"><?= esc_html__('מאפייני דירה','leos') ?></h5>
							<div class="form-row">
								<?php if($params): ?>
									<?php foreach($params as $p): ?>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="checkbox" id="param-<?= $p->term_id ?>" name='params[]' value="<?= $p->term_id ?>">
											<label class="form-check-label" for="inlineCheckbox1"><?= $p->name ?></label>
										</div>
									<?php endforeach; ?>
								<?php endif; ?>
							</div>
							<div class="row">
								<div class="form-group col-md-3 col-xl-3">
									<label for="inputFloor"><?= esc_html__('קומה','leos') ?></label>
									<div class="input-group">
										<input type="text" class="form-control" name="floor-from" id="inputFloor" placeholder="מ-">
										<input type="text" class="form-control" name="floor-to" id="inputFloor2" placeholder="עד-">
									</div>
								</div>
								<div class="form-group col-md-3 col-xl-3">
									<label for="inputSize"><?= esc_html__('גודל דירה (במ’’ר)','leos') ?></label>
									<div class="input-group">
										<input type="text" class="form-control" name="size-from" id="inputSize" placeholder="מ-">
										<input type="text" class="form-control" name="size-to" id="inputSize2" placeholder="עד-">
									</div>
								</div>
								<div class="form-group col-md-3 col-xl-3">
									<label for="inputDate"><?= esc_html__('תאריך כניסה','leos') ?></label>
									<input type="date" class="form-control" name="property-date" id="inputDate" name="inputDate">
								</div>
								<div class="form-check form-check-inline auto-width-check pt-4">
									<input class="form-check-input" type="checkbox" name="property-date-immediately" id="inlineCheckbox1" value="1">
									<label class="form-check-label" for="inlineCheckbox1"><?= esc_html__('כניסה מיידית','leos') ?></label>
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-4">
									<label for="inputFree"><?= esc_html__('חיפוש חופשי','leos') ?></label>
									<input type="text" name="search-query" class="form-control" id="inputFree">
								</div>
								<div class="form-check form-check-inline auto-width-check pr-min pt-4">
									<input class="form-check-input" type="checkbox" name="kibbutz_only" id="inlineCheckbox1" name="" value="1">
									<label class="form-check-label" for="inlineCheckbox1"><?= esc_html__('הצג רק מושבים וקיבוצים','leos') ?></label>
								</div>
							</div>
							<div class="form-group col-12 text-center position-relative mt-4">
								<button type="submit" class="btn btn-primary btn-search"><?= esc_html__('חיפוש','leos') ?></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
