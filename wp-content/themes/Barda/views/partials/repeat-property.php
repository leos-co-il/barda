<div class="add-property-block">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-auto d-flex align-items-center flex-wrap justify-content-center">
				<h3 class="add-property-big">
					<?= opt('add_prop_title'); ?>
				</h3>
				<a class="add-property-btn inverse-prop-btn" href="<?= opt('add_prop_link'); ?>">
					<span class="plus-icon light-plus">+</span>
					<span class="add-prop-text">פרסום נכס </span>
				</a>
			</div>
		</div>
	</div>
</div>
