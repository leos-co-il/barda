<?php if(isset($args['property']) && $args['property']) :
	$post_params = [
			['name' => 'rooms', 'title' => 'חדרים'],
			['name' => 'square', 'title' => 'מ"ר'],
			['name' => 'floor', 'title' => 'קומה'],
			['name' => 'storage', 'title' => 'מחסן'],
			['name' => 'balcony', 'title' => 'מרפסות'],
			['name' => 'perkings', 'title' => 'חניות'],
	];
?>
	<div class="property-item property-card" data-id="<?= $args['property']->ID; ?>">
		<div class="property-img" <?php if (has_post_thumbnail($args['property'])) : ?>
				style="background-image: url('<?= postThumb($args['property']); ?>')"
			<?php endif; ?>>
			<span class="heart-wrap add-to-wishlist" data-id="<?= $args['property']->ID; ?>"></span>
			<?php if ($tip = get_field('status', $args['property']->ID)) : ?>
				<div class="tip-wrap tip-<?= $tip['value']; ?>">
					<span class="tip-text"><?= $tip['label']; ?></span>
				</div>
			<?php endif; ?>
		</div>
		<div class="property-content">
			<div>
				<?php if ($price = get_field('price', $args['property']->ID)) : ?>
					<h3 class="property-price">
						₪<?= number_format($price); ?>
					</h3>
				<?php endif;
				if ($price = get_field('old_price', $args['property']->ID)) : ?>
					<h3 class="property-price property-price-old">
						₪<?= number_format($price); ?>
					</h3>
				<?php endif; ?>
				<h3 class="property-title mb-4 text-right"><?= $args['property']->post_title; ?></h3>
			</div>
			<div class="param-with-icons row">
				<?php foreach ($post_params as $par_item) : $par_name = $par_item['name'];
					if ($par_value = get_field($par_name, $args['property']->ID)) : ?>
						<div class="param-wrap col-xl-4 col-sm-6">
							<img src="<?= ICONS.$par_name.'.png'; ?>" class="param-icon" alt="<?= $par_name; ?>">
							<h3 class="param-title"><?= $par_value.' '.esc_html__($par_item['title'],'leos'); ?></h3>
						</div>
					<?php endif;
				endforeach; ?>
			</div>
			<a href="<?= $link; ?>" class="property-card-link">
				לחצו לפרטים
			</a>
		</div>
	</div>
<?php endif; ?>
