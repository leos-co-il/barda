<div class="top-page-wrap">
	<div class="top-page" <?php if (isset($args['img']) && $args['img']) : ?>
		style="background-image: url('<?= $args['img']; ?>')"
	<?php endif; ?>>
		<?php get_template_part('views/partials/form', 'search'); ?>
	</div>
	<?php if ( function_exists('yoast_breadcrumb') ) : ?>
		<div class="container">
			<div class="row justify-content-center bread-row">
				<div class="col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>
