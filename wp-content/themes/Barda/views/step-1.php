<?php
/*
Template Name: שלב 1
*/
the_post();
get_header();
?>
<article class="step-body-block first-step-block">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="block-title">1/<span>4</span>. פרסמו אצלנו נכס</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<form class="form-steps steps-first-form">
					<div class="form-row">
						<div class="col-9">
							<div class="row justify-content-between align-items-stretch">
								<div class="col-auto">
									<label class="big-label" for="type-own">איזה סוג מפרסם אתם ?</label>
									<div class="form-group links-group" id="type-own">
										<div class="input-group radio-group">
											<div class="mx-1 choose-option">
												<input type="radio" name="type-owner" class="radio-check">
												<img src="<?= ICONS ?>agent.png" alt="agent">
												<span>סוכן נדלן</span>
											</div>
											<div class="mx-1 choose-option">
												<input type="radio" name="type-owner" class="radio-check">
												<img src="<?= ICONS ?>owner.png" alt="owner">
												<span>פרטי</span>
											</div>
										</div>
									</div>
								</div>
								<div class="col-auto">
									<label class="big-label" for="type-own">איזה נכס תרצו לפרסם?</label>
									<div class="form-group links-group" id="type-own">
										<div class="input-group radio-group">
											<div class="mx-1 choose-option">
												<input type="radio" name="type-rental" class="radio-check">
												<img src="<?= ICONS ?>rent.png" alt="rent">
												<span>למכירה</span>
											</div>
											<div class="mx-1 choose-option">
												<input type="radio" name="type-rental" class="radio-check">
												<img src="<?= ICONS ?>buy.png" alt="buy">
												<span>להשכרה</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row align-items-end">
						<div class="col-lg-3 col-sm-6 col-in-form col-12">
							<label for="property-type">סוג הנכס</label>
							<select id="property-type" class="form-control">
								<option selected>בחרו סוג נכס</option>
							</select>
						</div>
						<div class="col-lg-3 col-sm-6 col-in-form col-12">
							<label for="search-place">חפשו עיר או ישוב</label>
							<input type="text" placeholder="לדוגמה: חיפה" id="search-place">
						</div>
						<div class="col-lg-3 col-sm-6 col-in-form col-12">
							<label for="search-street">חפשו רחוב</label>
							<input type="text" placeholder="לדוגמה: הלל" id="search-street">
						</div>
						<div class="col-lg-3 col-sm-6 col-in-form col-12">
							<label for="search-area">חפשו שכונה</label>
							<input type="text" placeholder="לדוגמה: רסקו" id="search-area">
						</div>
					</div>
					<div class="form-row align-items-end">
						<div class="col-lg-auto">
							<div class="form-row align-items-end">
								<div class="col-lg-auto col-md-6 col-in-form col-12">
									<label for="home-number">מספר בית</label>
									<input type="text" placeholder="הכנס מס" id="home-number">
								</div>
								<div class="col-lg col-md-6 col-in-form">
									<label for="property-condition">מצב נכס</label>
									<select id="property-condition" class="form-control">
										<option selected>בחר  מצב נכס</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-lg-auto col-12">
							<div class="form-row align-items-end">
								<div class="col-lg-5 col-in-form col-6 form-group">
									<label for="inputFloor">קומה</label>
									<div class="input-group">
										<input type="text" class="form-control" name="floor-from" id="inputFloor" placeholder="מ-">
										<input type="text" class="form-control" name="floor-to" id="inputFloor2" placeholder="מתוך-">
									</div>
								</div>
								<div class="col-lg col-6 d-flex">
									<div class="check-last pb-3">
										<input class="form-check-box" type="checkbox" id="on-pages">
										<label class="form-check-label" for="on-pages">
											על עמודים
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col-lg-auto">
							<div class="form-row align-items-end">
								<div class="col-md-auto">
									<label for="rooms">מספר חדרים</label>
									<select id="rooms" class="form-control">
										<option selected>הכנס מס</option>
									</select>
								</div>
								<div class="col-md-auto">
									<label for="balcony">מספר מרפסות</label>
									<select id="balcony" class="form-control">
										<option selected>הכנס מס</option>
									</select>
								</div>
								<div class="col-md-auto col-in-form col-12">
									<label for="balcony-size">גודל מרפסת במ”ר</label>
									<input type="text" placeholder="אופציה למילוי גודל" id="balcony-size">
								</div>
								<div class="col-md-auto">
									<label for="warehouses">מספר מחסנים</label>
									<select id="warehouses" class="form-control">
										<option selected>הכנס מס</option>
									</select>
								</div>
								<div class="col-md-auto col-in-form col-12">
									<label for="warehouses-size">גודל מחסן במ”ר</label>
									<input type="text" placeholder="אופציה למילוי גודל" id="warehouses-size">
								</div>
								<div class="form-group mb-2">
									<label for="check-group-position">סמנו כיווני אוויר </label>
									<div class="input-group">
										<div class="check-area">
											<div class="check-last">
												<input class="form-check-box" type="checkbox" id="loc-1">
												<label class="form-check-label" for="loc-1">
													צפון
												</label>
											</div>
										</div>
										<div class="check-area">
											<div class="check-last">
												<input class="form-check-box" type="checkbox" id="loc-2">
												<label class="form-check-label" for="loc-2">
													דרום
												</label>
											</div>
										</div>
										<div class="check-area">
											<div class="check-last">
												<input class="form-check-box" type="checkbox" id="loc-3">
												<label class="form-check-label" for="loc-3">
													מזרח
												</label>
											</div>
										</div>
										<div class="check-area">
											<div class="check-last">
												<input class="form-check-box" type="checkbox" id="loc-4">
												<label class="form-check-label" for="loc-4">
													מערב
												</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row form-row-properties">
						<div class="col-12">
							<h3 class="info-title bolder-middle">מאפייני דירה</h3>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="checkbox" id="param-1">
							<label class="form-check-label" for="param-1">מעלית</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="checkbox" id="param-2">
							<label class="form-check-label" for="param-2">מיזוג</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="checkbox" id="param-3">
							<label class="form-check-label" for="param-3">ממ”ד</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="checkbox" id="param-4">
							<label class="form-check-label" for="param-4">סורגים</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="checkbox" id="param-5">
							<label class="form-check-label" for="param-5">דוד שמש</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="checkbox" id="param-6">
							<label class="form-check-label" for="param-6">מרפסת</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="checkbox" id="param-7">
							<label class="form-check-label" for="param-7">מחסן</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="checkbox" id="param-8">
							<label class="form-check-label" for="param-8">מחמם מים בגז</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="checkbox" id="param-9">
							<label class="form-check-label" for="param-9">רשתות לחלון</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="checkbox" id="param-10">
							<label class="form-check-label" for="param-10">גישה לנכים </label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="checkbox" id="param-11">
							<label class="form-check-label" for="param-11">מטבח כשר</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="checkbox" id="param-12">
							<label class="form-check-label" for="param-12">חיות מחמד</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="checkbox" id="param-13">
							<label class="form-check-label" for="param-13">לשותפים </label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="checkbox" id="param-14">
							<label class="form-check-label" for="param-14">לטווח ארוך </label>
						</div>
					</div>
					<div class="form-row">
						<div class="col-12">
							<h3 class="info-title bolder-middle">נכס רשום בטאבו?</h3>
						</div>
						<div class="col-sm-1 col-6 d-flex">
							<div class="check-last">
								<input class="form-check-box" type="checkbox" id="yes">
								<label class="form-check-label" for="yes">
									כן
								</label>
							</div>
						</div>
						<div class="col-sm-1 col-6 d-flex">
							<div class="check-last">
								<input class="form-check-box" type="checkbox" id="no">
								<label class="form-check-label" for="no">
									לא
								</label>
							</div>
						</div>
						<div class="col-12">
							<span class="base-text remark">*פרטים אלו לא יוצגו במודעה*</span>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-9 col-12">
							<div class="form-row align-items-end">
								<div class="col-lg-4 col-in-form col-12">
									<label for="input-1">תשלום ועד</label>
									<input type="text" placeholder="הכניסו מספר כאן" id="input-1">
								</div>
								<div class="col-lg-4 col-in-form col-12">
									<label for="input-2">תשלום ארנונה</label>
									<input type="text" placeholder="הכניסו מספר כאן" id="input-2">
								</div>
								<div class="col-lg-4 col-in-form col-12">
									<label for="input-3">שנת בניית נכס</label>
									<input type="text" placeholder="הכניסו מספר כאן" id="input-3">
								</div>
								<div class="col-12">
									<span class="base-text remark">*פרטים אלו לא יוצגו במודעה* </span>
								</div>
							</div>
						</div>
						<div class="col-md-9 col-12">
							<h3 class="info-title bolder-middle">אודות הנכס</h3>
							<textarea placeholder="אזור טקסט מוגבל לעד 400 מילים "></textarea>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</article>
<?php get_footer('step'); ?>
