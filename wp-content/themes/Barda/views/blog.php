<?php
/*
Template Name: מאמרים
*/
the_post();
get_header();
$fields = get_fields();
$posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => 'post',
	'suppress_filters' => false,
]);
get_template_part('views/partials/content', 'top', [
		'img' => (isset($fields['top_img']) && $fields['top_img']) ? $fields['top_img']['url'] : (has_post_thumbnail() ? postThumb() : ''),
]); ?>
<article class="article-page-body page-body mt-4">
	<div class="container mb-100">
		<div class="row justify-content-start">
			<div class="col-auto">
				<h1 class="block-title"><?php the_title(); ?></h1>
				<div class="base-output">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row justify-content-center align-items-stretch put-here-posts">
				<?php foreach ($posts->posts as $i => $post) {
					get_template_part('views/partials/card', 'post', [
							'post' => $post,
					]);
				} ?>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php
get_template_part('views/partials/repeat', 'property');
get_template_part('views/partials/content', 'slider_props',
		[
				'items' => (isset($fields['same_props'])) ? $fields['same_props'] : '',
				'title' => isset($fields['same_props_title']) ? $fields['same_props_title'] : '',
]);
if (isset($fields['faq_block_item'])) {
	get_template_part('views/partials/content', 'faq', [
		'faq' => $fields['faq_block_item'],
		'title' => isset($fields['faq_title']) ? $fields['faq_title'] : '',
	]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
		[
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
		]);
}
get_footer(); ?>
