<?php
/*
Template Name: דף הבית
*/

the_post();
get_header();
$fields = get_fields();

?>

<section class="home-main" <?php if (has_post_thumbnail()) : ?>
	style="background-image: url('<?= postThumb(); ?>')"
 <?php endif; ?>>
	<?php get_template_part('views/partials/form', 'search'); ?>
</section>
<?php if ($fields['home_prop_slider']) : ?>
	<div class="sliders-home">
		<?php foreach ($fields['home_prop_slider'] as $x => $slider) : ?>
			<div class="sliders-wrapper-item">
				<?php get_template_part('views/partials/content', 'slider_props', [
						'items' => isset($slider['h_prop_slider_1']) ? $slider['h_prop_slider_1'] : '',
						'title' => isset($slider['h_prop_slider_title_1']) ? $slider['h_prop_slider_title_1'] : '',
						'link' => isset($slider['h_prop_slider_link_1']) ? $slider['h_prop_slider_link_1'] : '',
				]); ?>
			</div>
			<?php if ($x === 2) {
				get_template_part('views/partials/repeat', 'property');
			}
			if ($x === 6 && $fields['home_slider_seo']) {
				get_template_part('views/partials/content', 'slider',
						[
								'content' => $fields['home_slider_seo'],
								'img' => $fields['home_slider_img'],
						]);
			} ?>
		<?php endforeach; ?>
	</div>
<?php endif;
if ($fields['faq_block_item']) {
	get_template_part('views/partials/content', 'faq', [
			'faq' => $fields['faq_block_item'],
			'title' => $fields['faq_title'],
	]);
}
if ($fields['h_posts']) : ?>
	<section class="home-posts">
		<div class="container">
			<div class="row justify-content-center align-items-stretch arrows-slider big-arrows">
				<div class="col-12">
					<h2 class="block-title">
						<?= $fields['h_posts_title']; ?>
					</h2>
				</div>
				<?php foreach ($fields['h_posts'] as $post) {
					get_template_part('views/partials/card', 'post', [
							'post' => $post,
					]);
				}?>
			</div>
			<?php if ($fields['h_posts_link']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<a href="<?= $fields['h_posts_link']['url'];?>" class="base-link">
							<?= (isset($fields['h_posts_link']['title']) && $fields['h_posts_link']['title'])
									? $fields['h_posts_link']['title'] : 'לכל המאמרים';
							?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
			[
					'content' => $fields['single_slider_seo'],
					'img' => $fields['slider_img'],
			]);
}
get_footer(); ?>
