<?php
/*
Template Name: שלב 3
*/
the_post();
get_header();
?>
<article class="step-body-block third-step-body">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="block-title">3/<span>4</span>. פרסמו אצלנו נכס</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-5 col-lg-7 col-md-8 col-sm-10 col-12">
				<div class="base-output base-output-bigger">
					<p>
						לורם איפסום דולור סיט אמט, קונסקטוינג אלית גולר
						מונפרר סוברט לורם שבצק יהול, לכנוץ בע סולגק. בראיט ולחת צורק מורמי וסתלתכי
					</p>
					<p>
						דולור סיט אמט, קונסקטוינג אלית גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בע סולגק.
						בראיט ולחת צורק מונחף, בגורמי מגמש וסתלת
					</p>
				</div>
				<form>
					<div class="form-check">
						<input class="form-check-box" type="checkbox" id="check-1">
						<label class="form-check-label" for="check-1">
							אני מאשר/ת לורם היפסום לורם לורם עול, לכנוץ בע ולגק. בראיט ולחת צורק מ
						</label>
					</div>
					<div class="form-check">
						<input class="form-check-box" type="checkbox" id="check-2">
						<label class="form-check-label" for="check-2">
							<p>אני מאשר/ת לורם היפסום לורם לורם עול, לכנוץ בע
								ולגק. בראיט ולחת צורק מול, לכנוץ בע סולגק. בראיט ו מול,
								לכנוץ בע סולגק. בראיט ולחת צורק מולחת צורק מול, לכנוץ
							</p>
							<p>בע סולגק. בראיט ולחת צורק מול,
								לכנוץ בע סולגק. בראיט ולחת צורק מ</p>
						</label>
					</div>
					<label class="email-label">
						הכניסו לכאן מייל
						<input type="email" class="step-email-input" placeholder="מייל:" id="email-input">
					</label>
					<div class="form-check form-check-sm-m">
						<input class="form-check-box" type="checkbox" id="check-3">
						<label class="form-check-label" for="check-3">
							אני מאשר/ת לורם היפסום לורם לורם עול, לכנוץ בע
							ולגק. בראיט ולחת צורק מ
						</label>
					</div>
					<div class="form-check form-check-sm-m">
						<input class="form-check-box" type="checkbox" id="check-4">
						<label class="form-check-label" for="check-4">
							אני מאשר/ת לורם היפסום לורם לורם עול, לכנוץ בע
							ולגק. בראי
						</label>
					</div>
				</form>
			</div>
		</div>
	</div>
</article>
<?php get_footer('step'); ?>
