<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();
$tel = opt('tel');
$mail = opt('mail');
$whatsapp = opt('whatsapp');
$open_hours = opt('open_hours');
?>
<article class="page-body">
	<div class="contact-page-body">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h1 class="base-title"><?php the_title(); ?></h1>
				</div>
				<div class="col-12">
					<div class="base-output text-center">
						<?php the_content(); ?>
					</div>
				</div>
				<div class="col-12">
					<div class="box-wrapper box-wrapper-contact">
						<div class="row justify-content-center align-items-stretch">
							<div class="col-lg-6 col-12 col-contact-pad">
								<div class="contact-col h-100">
									<?php if ($tel) : ?>
										<a href="tel:<?= $tel; ?>" class="contact-item wow flipInX" data-wow-delay="0.2s">
											<div class="contact-icon-wrap">
												<img src="<?= ICONS ?>contact-tel.png" alt="phone">
											</div>
											<div class="contact-info">
												<h3 class="contact-type-title">התקשרו אלינו</h3>
												<h3 class="contact-type"><?= $tel; ?></h3>
											</div>
										</a>
									<?php endif; ?>
									<?php if ($whatsapp) : ?>
										<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" class="contact-item wow flipInX" data-wow-delay="0.4s">
											<div class="contact-icon-wrap">
												<img src="<?= ICONS ?>contact-whatsapp.png" alt="whatsapp">
											</div>
											<div class="contact-info">
												<h3 class="contact-type-title">אנחנו גם בוואצאפ</h3>
												<h3 class="contact-type"><?= $whatsapp; ?></h3>
											</div>
										</a>
									<?php endif;
									if ($mail) : ?>
										<a href="mailto:<?= $mail; ?>" class="contact-item wow flipInX" data-wow-delay="0.6s">
											<div class="contact-icon-wrap">
												<img src="<?= ICONS ?>contact-mail.png" alt="email">
											</div>
											<div class="contact-info">
												<h3 class="contact-type-title">כתבו לנו</h3>
												<h3 class="contact-type"><?= $mail; ?></h3>
											</div>
										</a>
									<?php endif;
									if ($open_hours) : ?>
										<div class="contact-item wow flipInX" data-wow-delay="0.8s">
											<div class="contact-icon-wrap">
												<img src="<?= ICONS ?>contact-hours.png">
											</div>
											<div class="contact-info">
												<h3 class="contact-type-title">השעות שלנו </h3>
												<h3 class="contact-type"><?= $open_hours; ?></h3>
											</div>
										</div>
									<?php endif; ?>
								</div>
							</div>
							<div class="col-lg-6 col-12">
								<div class="form-contact-wrap">
									<?php if ($fields['contact_form_title']) : ?>
										<h3 class="foo-form-title"><?= $fields['contact_form_title']; ?></h3>
									<?php endif;
									if ($fields['contact_form_title']) : ?>
										<h3 class="form-subtitle"><?= $fields['contact_form_subtitle']; ?></h3>
									<?php endif;
									getForm('86'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
