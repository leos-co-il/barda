<?php
/*
Template Name: שלב 2
*/
the_post();
get_header();
?>
<article class="step-body-block second-step-body">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="block-title">2/<span>4</span>. פרסמו/גררו לכאן תמונות</h1>
			</div>
		</div>
		<div class="row justify-content-between">
			<div class="col-lg-7">
				<div class="row">
					<div class="col-6 mb-4">
						<h3 class="info-title">תמונה ראשית</h3>
						<div class="downloaded-image"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<h3 class="info-title">תמונות נוספות</h3>
					</div>
					<div class="col-6">
						<div class="downloaded-image"></div>
					</div>
					<div class="col-6">
						<div class="downloaded-image"></div>
					</div>
					<div class="col-6">
						<div class="downloaded-image"></div>
					</div>
					<div class="col-6">
						<div class="downloaded-image"></div>
					</div>
					<div class="col-6">
						<div class="downloaded-image"></div>
					</div>
					<div class="col-6">
						<div class="downloaded-image"></div>
					</div>
				</div>
				<form>
					<div class="form-check">
						<input class="form-check-box" type="checkbox" id="check-accept">
						<label class="form-check-label" for="check-accept">
							פרסמתי תמונות ללא לוגו
						</label>
					</div>
				</form>
			</div>
			<div class="col-lg-4 col-md-8 col-sm-10 col-12">
				<h3 class="info-title">*חשוב לדעת*</h3>
				<div class="base-output base-output-bigger">
					<p>
						לורם איפסום דולור סיט אמט, קונסקטוינג אלית גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בע סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש וסתלתכי
					</p>
					<ul>
						<li>
							חונחף, בגורמי מגממושבצק יהול, לכנוץ בע סולגק.
						</li>
						<li>
							שי ומרגשחונחף, בגורמי מגממונצק יהול, לכנוץ בע סולגק.
						</li>
						<li>
							לחת צורק מונחףוסתלתכיט לכנוץ בע סולגק.
						</li>
						<li>
							בולחת צורק מונחף, בגורמי מגמש וסתלתכי
						</li>
					</ul>
					<p>
						מונפרר סוברט לורם שבצק יהול, לכנוץ בע סולגק.
						בראיט ולחת צורק מונחף, בגורמי מגמש וסתלתכי מונפרר סו ברט לורם שבצק יהול, לכנוץ בע סולגק.
						בראיט ולחת צורק מונחף, בגורמי מגמש וסתלתכי
					</p>
					<p>
						לורם איפסום דולור סיט אמט, וינג אלית גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בע סולגק. בראיט ולחת צוררמי מגמש וסתלתלורם איפסום
					</p>
					<p>
						דולור סיט אמט, קונסקטוינג אלית גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בע סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש וסתלת
					</p>
				</div>
				<h3 class="info-title">*תמונות עם לוגו לא יפורסמו ולא יאושרו*</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-7">
				<h3 class="info-title bolder-middle">פרטי הדירה </h3>
				<form class="form-steps">
					<div class="form-row align-items-end">
						<div class="col-sm-6 col-in-form col-12">
							<label for="input-1">גודל הנכס במ”ר</label>
							<input type="text" placeholder="אופציה למילוי גודל" id="input-1">
						</div>
						<div class="col-sm-6 col-in-form col-12">
							<label for="input-2">גודל הנכס במ”ר בנוי</label>
							<input type="text" placeholder="אופציה למילוי גודל" id="input-2">
						</div>
						<div class="col-sm-6 col-in-form col-12">
							<label for="input-3">מחיר הנכס בשקלים</label>
							<input type="text" placeholder="אופציה למילוי מחיר" id="input-3">
						</div>
						<div class="col-in-form col-sm-3 col-12">
							<label for="input-4">גתאריך כניסה</label>
							<input type="date" placeholder="הכנס תאריך " id="input-4">
						</div>
						<div class="col-sm col-6 d-flex">
							<div class="check-last">
								<input class="form-check-box" type="checkbox" id="type-1">
								<label class="form-check-label" for="type-1">
									גמיש
								</label>
							</div>
						</div>
						<div class="col-sm col-6 d-flex">
							<div class="check-last">
								<input class="form-check-box" type="checkbox" id="type-2">
								<label class="form-check-label" for="type-2">
									מיידי
								</label>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<img src="<?= IMG ?>second-step-back.png" alt="img" class="w-100 second-img-back">
</article>
<?php get_footer('step'); ?>
