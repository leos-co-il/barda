<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
$top_img = isset($fields['top_img']) ? $fields['top_img'] : opt('top_img');
?>

<article class="page-body post-body-page">
	<div class="top-page-wrap">
		<div class="top-page" <?php if ($top_img) : ?>
			style="background-image: url('<?= $top_img['url']; ?>')"
		<?php endif; ?>>
		</div>
		<?php if ( function_exists('yoast_breadcrumb') ) : ?>
			<div class="container">
				<div class="row justify-content-center bread-row">
					<div class="col-12">
						<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<div class="container mt-3">
		<div class="row justify-content-between">
			<div class="col-lg-8 col-12">
				<h1 class="block-title"><?php the_title(); ?></h1>
				<div class="base-output post-output">
					<?php the_content(); ?>
				</div>
			</div>
			<div class="col-lg-4 col-12 page-form-col-post">
				<div class="sticky-form">
					<div class="box-wrapper post-form-box">
						<div class="post-form">
							<?php if ($f_title = opt('post_form_title')) : ?>
								<h2 class="post-form-title"><?= $f_title; ?></h2>
							<?php endif;
							if ($f_subtitle = opt('post_form_subtitle')) : ?>
								<h3 class="post-form-subtitle"><?= $f_subtitle; ?></h3>
							<?php endif;
							getForm('87'); ?>
						</div>
					</div>
					<?php get_template_part('views/partials/repeat', 'share', [
							'link' => $post_link,
							'title' => $fields['faq_title'],
					]); ?>
				</div>
			</div>
		</div>
	</div>
</article>
<div class="mt-mb post-prop-add">
	<?php get_template_part('views/partials/repeat', 'property'); ?>
</div>
<?php
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
		'posts_per_page' => 3,
		'post_type' => 'post',
		'post__not_in' => array($postId),
		'tax_query' => [
				[
						'taxonomy' => 'category',
						'field' => 'term_id',
						'terms' => $post_terms,
				],
		],
]);
if (isset($fields['same_posts']) && $fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts == NULL) {
	$samePosts = get_posts([
			'posts_per_page' => 3,
			'orderby' => 'rand',
			'post_type' => 'post',
			'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
	<section class="home-posts same-posts">
		<div class="container">
			<div class="row justify-content-center align-items-stretch arrows-slider big-arrows">
				<div class="col-12">
					<h2 class="block-title">
						<?= isset($fields['same_title']) && $fields['same_title'] ? $fields['same_title'] : 'מאמרים נוספים בנושא'; ?>
					</h2>
				</div>
				<?php foreach ($samePosts as $post) {
					get_template_part('views/partials/card', 'post_same', [
							'post' => $post,
					]);
				}?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['faq_block_item']) {
	get_template_part('views/partials/content', 'faq', [
		'faq' => $fields['faq_block_item'],
		'title' => $fields['faq_title'],
	]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
