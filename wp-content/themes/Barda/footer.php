<?php

$tel = opt('tel');
$mail = opt('mail');
$fax = opt('fax');
$address = opt('address');
$open_hours = opt('open_hours');
$current_id = get_queried_object_id();
$contact_id = getPageByTemplate('views/contact.php');
$logo = opt('logo');
$id = get_the_ID();
?>

<footer>
	<?php if ($id !== getPageByTemplate('views/contact.php')) : ?>
		<div class="footer-form smaller-submit">
			<div class="container footer-container-menu">
				<div class="row justify-content-center">
					<div class="col-12">
						<div class="row form-foo-row align-items-center">
							<?php if ($logo = opt('logo')) : ?>
								<div class="col-lg-3 col-md-4 col-7">
									<a href="/" class="logo">
										<img src="<?= $logo['url'] ?>" alt="logo">
									</a>
								</div>
							<?php endif; ?>
							<div class="col-lg-9 col-md-8 col-12">
								<?php if ($title = opt('foo_form_title')) : ?>
									<h2 class="foo-form-title"><?= $title; ?></h2>
								<?php endif;
								if ($subtitle = opt('foo_form_subtitle')) : ?>
									<h3 class="form-subtitle mb-0"><?= $subtitle; ?></h3>
								<?php endif; ?>
							</div>
						</div>
						<div class="base-form-wrap">
							<?php getForm('85'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="footer-main">
		<a id="go-top">
			<span class="to-top-wrap">
				<img src="<?= ICONS ?>arrow-left-dark.png" alt="to-top">
				<img src="<?= ICONS ?>arrow-left-dark.png" alt="to-top">
			</span>
			<h5 class="top-text">
				חזרה למעלה
			</h5>
		</a>
		<div class="container footer-container-menu">
			<div class="row justify-content-center">
				<div class="col-lg-auto col-6 foo-menu">
					<h3 class="foo-title">תפריט ראשי</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-menu', '1'); ?>
					</div>
				</div>
				<div class="col-lg col-12 foo-menu foo-links-menu">
					<h3 class="foo-title"><?php
						$locations = get_nav_menu_locations();
						$menu_obj = wp_get_nav_menu_object($locations['footer-links-menu']);
						echo get_field('foo_menu_title', $menu_obj); ?></h3>
					<div class="menu-border-top">
						<?php getMenu('footer-links-menu', '1', 'hop-hey two-columns'); ?>
					</div>
				</div>
				<div class="col-lg-3 col-6 foo-menu foo-contacts-menu">
					<h3 class="foo-title">פרטי התקשרות</h3>
					<div class="menu-border-top">
						<ul class="contact-list">
							<?php  if ($address) : ?>
								<li>
									<a href="https://www.waze.com/ul?q=<?= $address; ?>" class="foo-link">
										<p class="foo-link-subtitle">
											<?= $address; ?>
										</p>
									</a>
								</li>
							<?php endif;
							if ($tel) : ?>
								<li>
									<a href="tel:<?= $tel; ?>" class="foo-link">
										<p class="foo-link-subtitle">
											<?= $tel; ?>
										</p>
									</a>
								</li>
							<?php endif;
							if ($fax) : ?>
								<li>
									<div class="foo-link fax-item">
										<p class="foo-link-subtitle">
											<?= $fax; ?>
										</p>
									</div>
								</li>
							<?php endif;
							if ($mail) : ?>
								<li>
									<a href="mailto:<?= $mail; ?>" class="foo-link">
										<p class="foo-link-subtitle"><?= $mail; ?></p>
									</a>
								</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>

<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
