<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
$title = get_the_title();
$terms_for_check = get_terms([
	'taxonomy' => 'param',
	'hide_empty' => false,
]);
$post_params = [
	['name' => 'rooms', 'title' => 'חדרים'],
	['name' => 'square', 'title' => 'מ"ר'],
	['name' => 'floor', 'title' => 'קומה'],
	['name' => 'storage', 'title' => 'מחסן'],
	['name' => 'balcony', 'title' => 'מרפסות'],
	['name' => 'perkings', 'title' => 'חניות'],
];
$postId = get_the_ID();
$post_terms_check = wp_get_object_terms($postId, 'param', ['fields' => 'ids']);
$post_terms = wp_get_object_terms( $postId, 'property_cat', ['fields'=>'ids'] );
?>

<?php get_template_part('views/partials/repeat', 'property'); ?>
<article class="property-body-block mt-3">
	<?php if ( function_exists('yoast_breadcrumb') ) : ?>
		<div class="container">
			<div class="row justify-content-center bread-row">
				<div class="col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container mt-1">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="box-wrapper mb-5">
					<div class="property-body-wrap">
						<div class="row justify-content-center">
							<?php if (isset($fields['prop_gallery']) && $fields['prop_gallery']) : ?>
								<div class="col-lg-6 col-12">
									<div class="prop-images-col arrows-slider white-arrows-slider">
										<div class="gallery-slider" dir="rtl">
											<?php foreach ($fields['prop_gallery'] as $x => $image) : ?>
												<div>
													<a class="gallery-slide" style="background-image: url('<?= $image['url']; ?>')"
													   href="<?= $image['url']; ?>" data-lightbox="images">
														<?php if ($fields['status']) : ?>
															<div class="tip-wrap tip-<?= $fields['status']['value']; ?>">
																<span class="tip-text"><?= $fields['status']['label']; ?></span>
															</div>
														<?php endif; ?>
														<span class="heart-wrap add-to-wishlist" data-id="<?= $postId; ?>"></span>
													</a>
												</div>
											<?php endforeach; ?>
										</div>
									</div>
								</div>
							<?php endif; ?>
							<div class="col mt-lg-0 mt-3">
								<div class="prop-info-wrap">
									<div class="bb-property">
										<div class="row align-items-center">
											<div class="col-sm-6 col-12">
												<?php if ($post_terms) : ?>
													<a class="property-title" href="<?= get_term_link($post_terms['0']); ?>">
														<?= get_term($post_terms['0'])->name; ?>
													</a>
												<?php endif; ?>
												<h1 class="property-title"><?= $title; ?></h1>
											</div>
											<div class="col-sm-6 col-12 property-price-wrap">
												<?php if ($fields['price']) : ?>
													<h4 class="property-price">
														₪<?= number_format($fields['price']); ?>
													</h4>
												<?php endif;
												if (isset($fields['old_price']) && $fields['old_price']) : ?>
													<h4 class="property-price property-price-old">
														₪<?= number_format($fields['price']); ?>
													</h4>
												<?php endif; ?>
											</div>
										</div>
									</div>
									<div class="param-with-icons row">
										<?php foreach ($post_params as $par_item) : $par_name = $par_item['name'];
											if ($par_value = $fields[$par_name]) : ?>
												<div class="param-wrap col-6">
													<img src="<?= ICONS.$par_name.'.png'; ?>" class="param-icon" alt="<?= $par_name; ?>">
													<h3 class="param-title"><?= $par_value.' '.esc_html__($par_item['title'],'leos'); ?></h3>
												</div>
											<?php endif;
										endforeach; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-12">
				<div class="row">
					<div class="col-xl-10">
						<h2 class="block-title d-block w-75">על הנכס</h2>
						<div class="base-output">
							<?php the_content(); ?>
						</div>
					</div>
					<div class="col-12 mt-4">
						<div class="check-property">
							<div class="row">
								<div class="col">
									<h2 class="block-title w-75">מה יש בנכס</h2>
								</div>
							</div>
							<div class="row">
								<?php foreach ($terms_for_check as $x_check => $check_box) : ?>
									<div class="col-xl-3 col-lg-4 col-md-3 col-sm-4 col-6 check-col">
										<div class="checkbox-wrap">
									<span class="checkbox-custom <?php echo $check_it = in_array($check_box->term_id, $post_terms_check) ? 'checked' : ''; ?>">
										<i class="fas fa-check"></i>
									</span>
											<h4 class="check-label"><?= $check_box->name; ?></h4>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-12">
				<div class="box-wrapper property-call">
					<div class="property-call-block">
						<h3 class="share-text">
							מפרסם המודעה
						</h3>
						<?php if (isset($fields['own_name']) && $fields['own_name']) : ?>
							<h3 class="own-name">
								<?= $fields['own_name']; ?>
							</h3>
						<?php endif; ?>
						<div class="d-flex flex-column justify-content-between align-items-stretch prop-own-links">
							<?php if (isset($fields['own_tel']) && $fields['own_tel']) : ?>
								<a href="tel:<?= $fields['own_tel']; ?>" class="own-link-contact">
									<img src="<?= ICONS ?>tel.png" alt="phone">
									<?= $fields['own_tel']; ?>
								</a>
							<?php endif;
							if (isset($fields['own_whatsapp']) && $fields['own_whatsapp']) : ?>
								<a href="https://api.whatsapp.com/send?phone=<?= $fields['own_whatsapp']; ?>" class="own-link-contact link-whatsapp">
									<i class="fab fa-whatsapp"></i>
									<?= $fields['own_whatsapp']; ?>
								</a>
							<?php endif; ?>
						</div>
						<?php if (isset($fields['own_text']) && $fields['own_text']) : ?>
							<p class="own-text">
								<?= $fields['own_text']; ?>
							</p>
						<?php endif; ?>
					</div>
				</div>
				<?php get_template_part('views/partials/repeat', 'share', [
					'link' => $post_link,
					'title' => $title,
					'type' => 'prop',
				]); ?>
			</div>
		</div>
	</div>
</article>
<?php
$properties_all = [];
$properties_all = get_posts([
	'posts_per_page' => 5,
	'post_type' => 'property',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'property_cat',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_props']) {
	$properties_all = $fields['same_props'];
} elseif ($properties_all == NULL) {
	$properties_all = get_posts([
		'posts_per_page' => 5,
		'orderby' => 'rand',
		'post_type' => 'property',
		'post__not_in' => array($postId),
	]);
}
$custom_title = $fields['same_props_title'] ? $fields['same_props_title'] : 'נכסים דומים אצלנו';
?>

<?php if ($properties_all) {
	get_template_part('views/partials/content', 'slider_props', [
			'title' => $custom_title,
			'items' => $properties_all,
	]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
		[
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
		]);
}
get_footer(); ?>
