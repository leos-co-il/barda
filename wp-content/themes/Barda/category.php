<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$query = get_queried_object();
$posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => 'post',
	'suppress_filters' => false,
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $query->term_id,
		]
	],
]);
$img = get_field('top_img', $query);
get_template_part('views/partials/content', 'top', [
	'img' => $img ? $img['url'] : (opt('top_img') ? opt('top_img')['url'] : ''),
]); ?>
<article class="article-page-body page-body mt-4">
	<div class="container mb-100">
		<div class="row justify-content-start">
			<div class="col-auto">
				<h1 class="block-title"><?= $query->name; ?></h1>
				<div class="base-output">
					<?= category_description(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row justify-content-center align-items-stretch put-here-posts">
				<?php foreach ($posts->posts as $i => $post) {
					get_template_part('views/partials/card', 'post', [
						'post' => $post,
					]);
				} ?>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php
get_template_part('views/partials/repeat', 'property');
$props = get_field('same_props', $query);
get_template_part('views/partials/content', 'slider_props',
	[
		'items' => $props ? $props : '',
		'title' => get_field('same_props_title', $query),
	]);
if ($faq = get_field('faq_block_item', $query)) {
	get_template_part('views/partials/content', 'faq', [
		'faq' => $faq,
		'title' => get_field('faq_title', $query),
	]);
}
if ($slider = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider',
		[
			'content' => $slider,
			'img' => get_field('slider_img', $query),
		]);
}
get_footer(); ?>
