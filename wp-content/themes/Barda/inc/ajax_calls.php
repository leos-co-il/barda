<?php

add_action('wp_ajax_nopriv_ajax_function', 'ajax_function');
add_action('wp_ajax_more_ajax_function', 'ajax_function');

function ajax_function()
{

    if (!wp_verify_nonce($_REQUEST['nonce'], "my_user_vote_nonce")) {
        exit("No naughty business please");
    }

    $result['type'] = "error";
    $result['var'] = 1;

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    } else {
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }
    die();
}
add_action('wp_ajax_nopriv_get_more_function', 'get_more_function');
add_action('wp_ajax_get_more_function', 'get_more_function');

function get_more_function() {
	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
	$id_term = (isset($_REQUEST['termID'])) ? $_REQUEST['termID'] : '';
	$id_page = (isset($_REQUEST['page'])) ? $_REQUEST['page'] : '';
	$ids = explode(',', $ids_string);
	$query = new WP_Query([
		'post_type' => 'post',
		'posts_per_page' => 3,
		'post__not_in' => $ids,
		'tax_query' => $id_term ? [
			[
				'taxonomy' => 'category',
				'field' => 'term_id',
				'terms' => $id_term,
			]
		] : '',
	]);
	$html = '';
	$result['html'] = '';
	if ($query->have_posts()) {
		foreach ($query->posts as $item) {
			$html = load_template_part('views/partials/card', 'post', [
				'post' => $item,
			]);
			$result['html'] .= $html;
		}
	}

	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}


function location_search()
{

	$result['type'] = "success";

	$terms = get_terms([
		'taxonomy' => 'location',
		'name__like' => $_REQUEST['q'],
		'hide_empty' => false
	]);

	$terms_string = [
		'city' => 'עיר',
		'neighborhood' => 'שכונה',
		'street' => 'רחוב',
		'kibbutz' => 'קיבוץ / ישוב'
	];

	if ($terms) {
		$_res = [
			'city' => [],
			'neighborhood' => [],
			'street' => [],
			'kibbutz' => []
		];
		foreach ($terms as $t) {
			$type = get_field('location_type', $t);
			$_res[$type][] = ['id' => $t->term_id, 'text' => $t->name];
		}


		foreach ($_res as $key => $_r) {
			if (!empty($_r)) {
				$result['data'][] = [
					'text' => $terms_string[$key],
					'children' => $_r
				];
			}
		}
	}


	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}

add_action('wp_ajax_nopriv_add_to_wish', 'add_to_wish');
add_action('wp_ajax_add_to_wish', 'add_to_wish');

function add_to_wish() {
	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
	$id_post = (isset($_REQUEST['wishID'])) ? $_REQUEST['wishID'] : '';
	$idsWish = explode(',', $ids_string);
	if (!in_array($id_post, $idsWish)) {
		$item = get_post($id_post);
		$html = '';
		$result['html'] = '';
		$html = load_template_part('views/partials/card', 'property', [
			'property' => $item,
		]);
		$result['html'] .= $html;
	}

	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}
