<?php
if(CATALOG){
    function product_post_type() {

        $labels = array(
            'name'                => 'מוצרים',
            'singular_name'       => 'מוצרים',
            'menu_name'           => 'מוצרים',
            'parent_item_colon'   => 'פריט אב:',
            'all_items'           => 'כל המוצרים',
            'view_item'           => 'הצג מוצר',
            'add_new_item'        => 'הוסף מוצר חדש',
            'add_new'             => 'הוסף חדש',
            'edit_item'           => 'ערוך מוצר',
            'update_item'         => 'עדכון מוצר',
            'search_items'        => 'חפש מוצר',
            'not_found'           => 'לא נמצא',
            'not_found_in_trash'  => 'לא מצא באשפה',
        );
        $rewrite = array(
            'slug'                => 'product',
            'with_front'          => true,
            'pages'               => true,
            'feeds'               => true,
        );
        $args = array(
            'label'               => 'product',
            'description'         => 'מוצרים',
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
            'taxonomies'          => array( 'product_cat' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-products',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'rewrite'             => $rewrite,
            'capability_type'     => 'post',
        );
        register_post_type( 'product', $args );

    }

    add_action( 'init', 'product_post_type', 0 );

    function product_taxonomy() {

        $labels = array(
            'name'                       => 'קטגוריות מוצרים',
            'singular_name'              => 'קטגוריות מוצרים',
            'menu_name'                  => 'קטגוריות מוצרים',
            'all_items'                  => 'כל הקטגוריות',
            'parent_item'                => 'קטגורית הורה',
            'parent_item_colon'          => 'קטגורית הורה:',
            'new_item_name'              => 'שם קטגוריה חדשה',
            'add_new_item'               => 'להוסיף קטגוריה חדשה',
            'edit_item'                  => 'ערוך קטגוריה',
            'update_item'                => 'עדכן קטגוריה',
            'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
            'search_items'               => 'חיפוש קטגוריות',
            'add_or_remove_items'        => 'להוסיף או להסיר קטגוריות',
            'choose_from_most_used'      => 'בחר מהקטגוריות הנפוצות ביותר',
            'not_found'                  => 'לא נמצא',
        );
        $rewrite = array(
            'slug'                       => 'product_cat',
            'with_front'                 => true,
            'hierarchical'               => false,
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'rewrite'                    => $rewrite,
        );
        register_taxonomy( 'product_cat', array( 'product' ), $args );

    }

    add_action( 'init', 'product_taxonomy', 0 );
}

if(PROJECTS){
    function project_post_type() {

        $labels = array(
            'name'                => 'פרויקטים',
            'singular_name'       => 'פרויקטים',
            'menu_name'           => 'פרויקטים',
            'parent_item_colon'   => 'פריט אב:',
            'all_items'           => 'כל הפרויקטים',
            'view_item'           => 'הצג פרויקט',
            'add_new_item'        => 'הוסף פרויקט',
            'add_new'             => 'הוסף פרויקט חדש',
            'edit_item'           => 'ערוך פרויקט',
            'update_item'         => 'עדכון פרויקט',
            'search_items'        => 'חפש פריטים',
            'not_found'           => 'לא נמצא',
            'not_found_in_trash'  => 'לא מצא באשפה',
        );
        $rewrite = array(
            'slug'                => 'project',
            'with_front'          => true,
            'pages'               => true,
            'feeds'               => true,
        );
        $args = array(
            'label'               => 'project',
            'description'         => 'פרויקטים',
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
            'taxonomies'          => array( 'project_cat' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-portfolio',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'rewrite'             => $rewrite,
            'capability_type'     => 'post',
        );
        register_post_type( 'project', $args );

    }

    add_action( 'init', 'project_post_type', 0 );

    function project_taxonomy() {

        $labels = array(
            'name'                       => 'קטגוריות פרויקטים',
            'singular_name'              => 'קטגוריות פרויקטים',
            'menu_name'                  => 'קטגוריות פרויקטים',
            'all_items'                  => 'כל הקטגוריות',
            'parent_item'                => 'קטגורית הורה',
            'parent_item_colon'          => 'קטגורית הורה:',
            'new_item_name'              => 'שם קטגוריה חדשה',
            'add_new_item'               => 'להוסיף קטגוריה חדשה',
            'edit_item'                  => 'ערוך קטגוריה',
            'update_item'                => 'עדכן קטגוריה',
            'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
            'search_items'               => 'חיפוש קטגוריות',
            'add_or_remove_items'        => 'להוסיף או להסיר קטגוריות',
            'choose_from_most_used'      => 'בחר מהקטגוריות הנפוצות ביותר',
            'not_found'                  => 'לא נמצא',
        );
        $rewrite = array(
            'slug'                       => 'project_cat',
            'with_front'                 => true,
            'hierarchical'               => false,
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'rewrite'                    => $rewrite,
        );
        register_taxonomy( 'project_cat', array( 'project' ), $args );

    }

    add_action( 'init', 'project_taxonomy', 0 );
}

if (PROPERTIES) {
	function property_post_type() {

		$labels = array(
			'name'                => 'נכסים',
			'singular_name'       => 'נכסים',
			'menu_name'           => 'נכסים',
			'parent_item_colon'   => 'פריט אב:',
			'all_items'           => 'כל הנכסים',
			'view_item'           => 'הצג נכס',
			'add_new_item'        => 'הוסף נכס חדש',
			'add_new'             => 'הוסף חדש',
			'edit_item'           => 'ערוך נכס',
			'update_item'         => 'עדכון נכס',
			'search_items'        => 'חפש נכס',
			'not_found'           => 'לא נמצא',
			'not_found_in_trash'  => 'לא מצא באשפה',
		);
		$rewrite = array(
			'slug'                => 'property',
			'with_front'          => true,
			'pages'               => true,
			'feeds'               => true,
		);
		$args = array(
			'label'               => 'property',
			'description'         => 'נכסים',
			'labels'              => $labels,
			'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
			'taxonomies'          => array( 'property_cat', 'param' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-building',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'rewrite'             => $rewrite,
			'capability_type'     => 'post',
		);
		register_post_type( 'property', $args );

	}
	add_action( 'init', 'property_post_type', 0 );
	function location_taxonomy() {

		$labels = array(
			'name'                          => 'כתובות',
			'singular_name'                 => 'כתובות',
			'menu_name'                     => 'כתובות',
			'all_items'                     => 'כל הכתובות',
			'parent_item'                   => 'ראשי',
			'parent_item_colon'             => 'עיר',
			'new_item_name'                 => 'חדש',
			'add_new_item'                  => 'הוסף',
			'edit_item'                     => 'ערוך',
			'update_item'                   => 'עדכן',
			'separate_items_with_commas'    => 'קטגוריות נפרדות עם פסיק',
			'search_items'                  => 'חיפוש',
			'add_or_remove_items'           => 'להוסיף או להסיר קטגוריות',
			'choose_from_most_used'         => 'בחר',
			'not_found'                     => 'לא נמצא',
		);
		$rewrite = array(
			'slug'                          => 'location',
			'with_front'                    => true,
			'hierarchical'                  => false,
		);
		$args = array(
			'labels'                        => $labels,
			'hierarchical'                  => true,
			'public'                        => true,
			'show_ui'                       => true,
			'show_admin_column'             => true,
			'show_in_nav_menus'             => true,
			'show_tagcloud'                 => true,
			'rewrite'                       => $rewrite,
		);
		register_taxonomy('location', array('property'), $args);

	}
	add_action('init', 'location_taxonomy', 0);

	function property_params() {
		$labels = array(
			'name'                       => 'מאפייני הנכס',
			'singular_name'              => 'מאפייני הנכס',
			'menu_name'                  => 'מאפייני הנכס',
			'all_items'                  => 'כל מאפיינים',
			'new_item_name'              => 'חדש',
			'add_new_item'               => 'הוספת חדש',
			'edit_item'                  => 'ערוך',
			'update_item'                => 'עדכן',
			'search_items'               => 'חיפוש',
			'add_or_remove_items'        => 'להוסיף או להסיר ',
			'not_found'                  => 'לא נמצא',
		);
		$rewrite = array(
			'slug'                       => 'param',
			'with_front'                 => false,
			'hierarchical'               => false,
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => false,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => $rewrite,
		);
		register_taxonomy( 'param', array( 'property' ), $args );
	}
	add_action( 'init', 'property_params', 0 );

	function property_taxonomy() {

		$labels = array(
			'name'                       => 'קטגוריות נכסים',
			'singular_name'              => 'קטגוריות נכסים',
			'menu_name'                  => 'קטגוריות נכסים',
			'all_items'                  => 'כל הקטגוריות',
			'parent_item'                => 'קטגורית הורה',
			'parent_item_colon'          => 'קטגורית הורה:',
			'new_item_name'              => 'שם קטגוריה חדשה',
			'add_new_item'               => 'להוסיף קטגוריה חדשה',
			'edit_item'                  => 'ערוך קטגוריה',
			'update_item'                => 'עדכן קטגוריה',
			'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
			'search_items'               => 'חיפוש קטגוריות',
			'add_or_remove_items'        => 'להוסיף או להסיר קטגוריות',
			'choose_from_most_used'      => 'בחר מהקטגוריות הנפוצות ביותר',
			'not_found'                  => 'לא נמצא',
		);
		$rewrite = array(
			'slug'                       => 'property_cat',
			'with_front'                 => true,
			'hierarchical'               => false,
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => $rewrite,
		);
		register_taxonomy( 'property_cat', array( 'property' ), $args );

	}
	add_action( 'init', 'property_taxonomy', 0 );
	function property_type() {

		$labels = array(
			'name'                       => 'סוגי נכס',
			'singular_name'              => 'סוג נכס',
			'menu_name'                  => 'סוגי נכס',
			'all_items'                  => 'כל הסוגים',
			'parent_item'                => 'סוג הורה',
			'parent_item_colon'          => 'סוג הורה:',
			'new_item_name'              => 'שם סוג חדש',
			'add_new_item'               => 'להוסיף סוג חדש',
			'edit_item'                  => 'ערוך סוג',
			'update_item'                => 'עדכן סוג',
			'separate_items_with_commas' => 'סוגים נפרדות עם פסיק',
			'search_items'               => 'חיפוש סוגים',
			'add_or_remove_items'        => 'להוסיף או להסיר סוגים',
			'choose_from_most_used'      => 'בחר מהסוגים הנפוצות ביותר',
			'not_found'                  => 'לא נמצא',
		);
		$rewrite = array(
			'slug'                       => 'property_type',
			'with_front'                 => true,
			'hierarchical'               => false,
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => $rewrite,
		);
		register_taxonomy( 'property_type', array( 'property' ), $args );

	}
	add_action( 'init', 'property_type', 0 );
}
