<?php

get_header();
$term = get_queried_object();
$children = get_terms( $term->taxonomy, [
	'parent'    => $term->term_id,
	'hide_empty' => false
]);
$posts = get_posts([
	'numberposts' => -1,
	'post_type' => 'property',
	'tax_query' => [
		[
			'taxonomy' => 'property_cat',
			'field' => 'term_id',
			'terms' => $term->term_id,
		]
	]
]);
$img = get_field('top_img', $term);
get_template_part('views/partials/content', 'top', [
	'img' => $img ? $img['url'] : (opt('top_img') ? opt('top_img')['url'] : ''),
]); ?>
<article class="article-page-body page-body properties-search-body">
	<div class="container">
		<div class="row justify-content-start">
			<div class="col-auto">
				<h1 class="block-title"><?= $term->name; ?></h1>
				<div class="base-output">
					<?= category_description(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($children) : ?>
		<div class="padding-no">
			<div class="posts-output">
				<div class="container">
					<div class="row justify-content-center align-items-stretch">
						<?php foreach ($children as $i => $post) {
							get_template_part('views/partials/card', 'category', [
								'category' => $post,
							]);
						} ?>
					</div>
				</div>
			</div>
		</div>
	<?php else : ?>
		<div class="cats-list mb-5">
			<div class="container">
				<div class="row justify-content-center">
					<?php if ($posts) : ?>
						<div class="col-12">
							<div class="row justify-content-center align-items-stretch">
								<?php foreach ($posts as $i => $post) : ?>
									<div class="col-12 property-col">
										<?php get_template_part('views/partials/card', 'property_inline', [
											'property' => $post,
										]); ?>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					<?php else: ?>
						<div class="col-12">
							<div class="row justify-content-center">
								<div class="col">
									<h3 class="base-title text-center">אין נכסים בקטגוריה</h3>
								</div>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php
get_template_part('views/partials/repeat', 'property');
$props = get_field('same_props', $term);
get_template_part('views/partials/content', 'slider_props',
	[
		'items' => $props ? $props : '',
		'title' => get_field('same_props_title', $term),
	]);
if ($faq = get_field('faq_block_item', $term)) {
	get_template_part('views/partials/content', 'faq', [
		'faq' => $faq,
		'title' => get_field('faq_title', $term),
	]);
}
if ($slider = get_field('single_slider_seo', $term)) {
	get_template_part('views/partials/content', 'slider',
		[
			'content' => $slider,
			'img' => get_field('slider_img', $term),
		]);
}
get_footer(); ?>
