<footer>
	<div class="step-footer">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="steps-btn-wrap">
						<button class="step-go step-prev">
							<img src="<?= ICONS ?>next.png" alt="prev-step">
							חזרה לאתר
						</button>
						<button class="step-go step-next">
							פרסם נכס נוסף
							<img src="<?= ICONS ?>prev.png" alt="next-step">
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
